var express = require('express');
var shell = require('shelljs');
var fs = require('fs');
var path = require('path');
var bodyParser = require('body-parser');
const exec = require('child_process').exec;
const cwd = __dirname;
var app = express();

// setup headers for domain cross communication
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS');
  res.header("Access-Control-Allow-Headers", "X-Requested-With, Origin, Content-Type, Accept");
  next();
});

// parse application/json
app.use(bodyParser.json());

/**
 * Function to create Python Unittest test template
 */
function pyUnittestTemplate(pName, t) {
    
    var temp = "# !/usr/bin/env python\n# -*- coding: utf-8 -*-\n\n";
    temp += "import unittest\n\n";
    temp += "class Test" + pName + "(unittest.TestCase):\n\n";
    for (var i = 0; i < t.length; i++) {
        temp += '    def test_' + t[i].name + '(self):\n';
        if (t[i].lines && t[i].lines.length > 0) {
            temp += '        """Selected requirements in document:\n';
            for (var j = 0; j < t[i].lines.length; j++) {
                temp += "        " + j + " '" + t[i].lines[j].content + "'\n";
            }
        } else {
            temp += '        """\n';
            temp += '        no selected requirements for this test case\n';
        }
        temp += '        """\n        # write your code here\n';
        temp += '        self.assertTrue(False)\n\n';
    }
    temp += "if __name__ == '__main__':\n";
    temp += "    unittest.main()\n";
    return temp;

}

/**
 * Function returns array of names of saved projects
 */
function getSavedProjects() {

    var savedProjects = [],
        dirs = fs.readdirSync(cwd + '/projects');
    for (var i = 0; i < dirs.length; i++) {
        if (dirs[i][0] === '.') { continue }
        try {
            fs.accessSync(cwd + '/projects/' + dirs[i] + "/project.json", fs.F_OK);
            savedProjects.push(dirs[i]);
        } catch (e) {
            continue;
        }
    }
    return savedProjects;

}

/**
 * Function returns boolean value if project exists in project structure
 */
function projectExists(pName) {

    // get list of all directories in 'projects' directory
    var dirs = fs.readdirSync(cwd + '/projects');
    for (var i = 0; i < dirs.length; i++) {
        // skip MacOSX' '.ds_store' folder
        if (dirs[i][0] === '.') { continue }
        // true if dir's name equals to given project's name
        if (dirs[i] === pName) {
            return true;
        }
    }
    // not found == not exists
    return false;

}

/**
 * REST API access point "/create"
 * - creates project structure
 */
app.get('/create', function(req, res) {

    var projectName = req.query.projectName;
    // if project does not exist yet, it is created with given name
    if (!projectExists(projectName)) {
        shell.mkdir(cwd + '/projects/' + projectName);
        shell.mkdir(cwd + '/projects/' + projectName + "/tests");
        console.log("-> " + projectName + " created");
        res.send({ created: true });
    // if project does already exists
    } else {
        console.log("-> " + projectName + " already exists");
        res.send({ created: false });
    }

});

/**
 * REST API access point "/delete"
 * - removes project structure
 */
app.get('/delete', function(req, res) {

    // if project structure exists, it will be removed
    if (projectExists(req.query.projectName)) {
        shell.rm('-rf', cwd + "/projects/" + req.query.projectName);
        console.log("-> " + req.query.projectName + " deleted");
        res.send({ deleted: true });
    // if project not exist, a message is sent to client that the project does not exist
    } else {
        console.log("-> " + req.query.projectName + " not exists");
        res.send({ deleted: false });
    }

});

/**
 * REST API access point "/save"
 * - saved actual state of project on client side of AFRET to a .json file
 */
app.post('/save', function(req, res) {

    var projectFile = cwd + '/projects/' + req.body.project.name + '/project.json';
    projectData = JSON.stringify({
        project: req.body.project,
        tests: req.body.tests
    });
    // write project content with all its tests
    fs.writeFile(projectFile, projectData, function(err) {
        if (err) {
            console.log("-> " + req.body.project.name + " save fail -> '" + err + "'");
            res.send({
                projectId: req.body.project._id,
                saved: false
            });
        } else {
            console.log("-> " + req.body.project.name + " saved");
            res.send({
                projectId: req.body.project._id,
                saved: true
            });
        }
    });

});

/**
 * REST API access point "/savedProjects"
 * - return array of saved project to AFRET client
 */
app.get('/savedProjects', function(req, res) {

    var savedProjects = getSavedProjects();
    console.log("-> savedProjects request");
    res.send({ savedProjects: savedProjects });

});

/**
 *  REST API access point "load"
 *  - sends project.json file of chosen project to AFRET client
 */
app.get('/load', function(req, res) {

    var projectObj = JSON.parse(fs.readFileSync(
        cwd + '/projects/' + req.query.projectName + "/project.json"
    ));
    res.send(projectObj);
    console.log("-> " + req.query.projectName + " loaded");

});

/**
 * REST API access point "/template"
 * - creates test template of a project in a given language
 */
app.post('/template', function (req, res) {

    var template = "",
        err = false,
        project = req.body.project,
        tests = req.body.tests,
        testFile = cwd + '/projects/' + project.name + '/tests/test';
    // branching by programming language
    if (project.language === "Python Unittest") {
        template = pyUnittestTemplate(project.name, tests);
        try {
            fs.writeFileSync(testFile + '.py', template, 'utf8');
        } catch (e) {
            err = true;
        }
    }
    else {
        // other programming language templates
    }
    if (!err) {
        console.log('-> ' + project.name + ' template created');
        res.send({
            projectId: project.id,
            templated: true
        });
    } else {
        console.log('-> ' + project.name + ' templated not created');
        res.send({
            templated: false
        });
    }

});

/**
 * REST API access point "/execute"
 * - executes test suite and returns test result
 */
app.get('/execute', function (req, res) {
    
    var projectName = req.query.projectName,
        runCmd = "",
        testName = "",
        testPath = cwd + '/projects/' + projectName + '/tests',
        testsDirFiles = fs.readdirSync(testPath);
    // iterate through all files in "/projects/:project/tests" dir and pick "test.py" file
    for (var i = 0; i < testsDirFiles.length; i++) {
        if (testsDirFiles[i].match(/^test\.(py)$/) !== null) {
            testName = testsDirFiles[i];
            break;
        }
    }
    // check if found the file
    if (!testName) {
        res.send({
            projectName: projectName,
            executed: false,
            err: true,
            msg: 'test.* not found'
        });
    // file found
    } else {
        if (path.extname(testName) === '.py') {
            runCmd = "python -m unittest discover -s " + testPath + " -p test.py";
        } else {
            console.log("another language run cmd");
            res.send({
                projectName: projectName,
                executed: false,
                err: true,
                msg: 'not supported language'
            });
        }
        // run execution command and retrieve results
        exec(runCmd, function(code, stdout, stderr) {
            var stderrLines = stderr.split('\n'),
                failedTests = [],
                testErrors = [],
                respObj = {};
            respObj.projectName = projectName;
            /* Set status according to first line */            
            // global error
            if (stderrLines[0].match(/^E$/)) {
                respObj.status = "ERROR";
            // no error
            } else {
                respObj.status = (stderrLines[0].match(/(F|E)/)) ? "FAIL" : "PASS";
                respObj.totalTests = stderrLines[0].length;
            }
            /* Collect info about test  */
            for (var i = 1; i < stderrLines.length; i++) {
                if (respObj.status === "PASS") {
                    // everything OK, all tests pass
                    break;
                } else if (respObj.status === "FAIL") {
                    if (stderrLines[i].match(/^ERROR:/)) {
                        // get name of test, where the error happened
                        var errObj = {
                            testName: stderrLines[i].match(/(?=(test_))\w+/)[0].substring(5)
                        };
                        // get test's error
                        for (i += 1; i < stderrLines.length; i++) {
                            if (stderrLines[i].match(/\w+Error:/)) {
                                errObj.error = stderrLines[i];
                                break;
                            }
                        }
                        testErrors.push(errObj);
                    } else if (stderrLines[i].match(/^FAIL:/)) {
                        // get name of failed test
                        var failObj = {
                            testName: stderrLines[i].match(/(?=(test_))\w+/)[0].substring(5)
                        };
                        // get test's assertion error
                        for (i += 1; i < stderrLines.length; i++) {
                            if (stderrLines[i].match(/\w+Error:/)) {
                                failObj.assert = stderrLines[i];
                                break;
                            }
                        }
                        failedTests.push(failObj);
                    }
                } else { // if status === 'ERROR'
                    if (stderrLines[i].match(/^ERROR:/)) {
                        for (i += 1; i < stderrLines.length; i++) {
                            if (stderrLines[i].match(/\w+Error:/)) {
                                respObj.error = stderrLines[i];
                                break;
                            }
                        }
                    }
                }
            }
            if (respObj.status === "FAIL") {
                respObj.testErrors = testErrors;
                respObj.failedTests = failedTests;
            }
            console.log("-> " + projectName + " executed");
            res.send(respObj);
        });
    }

});

// say hello messages on server start
console.log("AFRET_AGENT listening Your requests");
app.listen(3001);
